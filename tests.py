import unittest
from stubs import ThirdTaskStubs


class SegmentTests(unittest.TestCase):
    def test_intersecting_true(self):
        segment_1 = ThirdTaskStubs.INTERSECTING_SEGMENTS[0]
        segment_2 = ThirdTaskStubs.INTERSECTING_SEGMENTS[1]

        self.assertEqual(True, segment_1.is_intersecting(segment_2),
                         'Отрезки должны пересекаться')

    def test_intersecting_false(self):
        segment_1 = ThirdTaskStubs.NON_INTERSECTING_SEGMENTS[0]
        segment_2 = ThirdTaskStubs.NON_INTERSECTING_SEGMENTS[1]

        self.assertEqual(False, segment_1.is_intersecting(segment_2),
                         'Отрезки не должны пересекаться')


class PolygonTests(unittest.TestCase):
    def setUp(self):
        pass

    def test_is_triangle(self):
        self.assertEqual(True, ThirdTaskStubs.TRIANGLE.is_triangle(),
                         'Должен быть триугольником')
        self.assertEqual(False, ThirdTaskStubs.CONVEX_POLYGON_1.is_triangle(),
                         'Должен быть не триугольником')

    def test_get_non_convex_vertex(self):
        self.assertEqual(None, ThirdTaskStubs.TRIANGLE.get_non_convex_vertex(),
                         'Треугольник должен быть выпуклым')

        self.assertEqual(None, ThirdTaskStubs.CONVEX_POLYGON_1.get_non_convex_vertex(),
                         'Многоугольник должен быть выпуклым')

        self.assertEqual(2, ThirdTaskStubs.NON_CONVEX_POLYGON_1.get_non_convex_vertex(),
                         'Многоугольник должен быть невыпуклым')

    def test_is_inside(self):
        inside_points = [ThirdTaskStubs.SEGMENT_INSIDE_CONVEX_POLYGON_1.start_point,
                         ThirdTaskStubs.SEGMENT_INSIDE_CONVEX_POLYGON_1.end_point]
        outside_points = [ThirdTaskStubs.SEGMENT_OUTSIDE_CONVEX_POLYGON_1.start_point,
                          ThirdTaskStubs.SEGMENT_OUTSIDE_CONVEX_POLYGON_1.end_point]

        for point in inside_points:
            self.assertEqual(True, ThirdTaskStubs.CONVEX_POLYGON_1.is_inside(point),
                             'Точка должна быть внутри многоугольника')

        for point in outside_points:
            self.assertEqual(False, ThirdTaskStubs.CONVEX_POLYGON_1.is_inside(point),
                             'Точка должна быть вне многоугольника')
