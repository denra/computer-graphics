from PIL import Image
from math import sqrt
import sys
from functools import partial


HEIGHT = 800
WIDTH = 600

MW = WIDTH // 2
MH = HEIGHT // 2

WHITE = (255, 255, 255)
BLACK = (0, 0, 0)
RED = (255, 0, 0)
BLUE = (26, 45, 196)


def draw_point(xx, yy, pixels):
    if not (-MW < xx < MW and -MH < yy < MH):
        return False

    pixels[xx + MW, MH - yy] = RED
    return True


def is_busy(xx, yy, pixels):
    if not (-MW < xx < MW and -MH < yy < MH):
        return False

    return pixels[xx + MW, MH - yy] == RED


def get_p(a, b, c):
    """Если p < 0, то ветви в нижний правый угол, иначе в левый верхний"""
    if c == 0:
        raise BadArgsException('c == 0')
    return sqrt(2) * (a - b) / (8 * c)


def retransform(x_2, y_2, a, b, c):
    """:return x, y"""
    if c == 0 or a == b:
        raise BadArgsException('c == 0 or a == b')

    x = sqrt(2) / 2 * (x_2 - y_2 - sqrt(2) * (a + b) * (3 * b - a) / (16 * c * (b - a)))
    y = sqrt(2) / 2 * (x_2 + y_2 + sqrt(2) * (a + b) * (3 * a - b) / (16 * c * (b - a)))
    return x, y


def transform(x, y):
    """:return x_2, y_2"""
    pass


def get_vertex(a, b, c):
    return retransform(0, 0, a, b, c)


def get_focus_dist(x0, y0, a, b, c):
    """ Расстояние до директрисы

    :param x0: в исходных координатах
    :param y0: в исходных координатах
    """
    if c == 0 or a == b:
        raise BadArgsException('c == 0 or a == b')

    focus_x, focus_y = retransform(0, sqrt(2) * (a - b) / (16 * c), a, b, c)

    return sqrt((x0 - focus_x) * (x0 - focus_x) + (y0 - focus_y) * (y0 - focus_y))


def get_directrix_dist(x0, y0, a, b, c):
    """ Расстояние до директрисы

    :param x0: в исходных координатах
    :param y0: в исходных координатах
    """
    if c == 0 or a == b:
        raise BadArgsException('c == 0 or a == b')
    return abs(sqrt(2) / 2 * (y0 - x0 + (a * a + b * b) / (4 * c * (a - b))))


def get_score(x, y, a, b, c):
    return abs(get_focus_dist(x, y, a, b, c) - get_directrix_dist(x, y, a, b, c))


def get_min_shift(x, y, shifts, scorer):
    min_shift = shifts[0]
    min_score = scorer(coords[0][0], coords[0][1])

    for coord in coords:
        tmp_score = scorer(coord[0], coord[1])
        if tmp_score < min_score:
            min_coord = coord
            min_score = tmp_score
    print(min_score)
    return min_coord


def brez(a=1, b=2, c=3):
    img = Image.new('RGB', (WIDTH, HEIGHT), color=WHITE)
    pixels = img.load()
    p = get_p(a, b, c)
    _get_score = partial(get_score, a=a, b=b, c=c)

    x, y = get_vertex(a, b, c)
    draw_point(x, y, pixels)

    if p < 0:  # Правый нижний угол
        #     Левая ветка
        # while x < MW and -MH < y < MH:
        #     coords = [(x, y + 1), (x + 1, y + 1), (x + 1, y), (x + 1, y - 1), (x, y - 1)]
        #     min_coord = get_min_coord(coords, _get_score, pixels)
        #     draw_point(min_coord[0], min_coord[1], pixels)
        #     x, y = min_coord

        #     Правая ветка
        while x < MW and -MH < y < MH:
            shifts = [(-1, -1), (0, -1), (1, -1)]
            min_coord = get_min_shift(x, y, shifts, _get_score)
            draw_point(min_coord[0], min_coord[1], pixels)
            x, y = min_coord
    else:
        print('todo')
        sys.exit()

    img.show()

# while x < MW:
#         delta = simple_f(x + 1, y + 1, p)
#         if delta > 0:  # вне купола параболы
#             # либо на диагонали, либо шаг вправо
#             error = simple_f(x + 1, y, p)
#             if abs(delta) > abs(error):
#                 x = x + 1
#                 y = y
#             else:
#                 x = x + 1
#                 y = y + 1
#         else:
#             # либо на диагонали, либо шаг вверх
#             error = simple_f(x, y - 1, p)
#             if abs(delta) > abs(error):
#                 x = x
#                 y = y + 1
#             else:
#                 x = x + 1
#                 y = y + 1
#         draw_sym_points(x, y, pixels)

class BadArgsException(Exception):
    pass


if __name__ == '__main__':
    brez()
