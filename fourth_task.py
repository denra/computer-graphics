from PIL import Image
import math

my = HEIGHT = 800
mx = WIDTH = 600

WHITE = (255, 255, 255)
BLACK = (0, 0, 0)
RED = (255, 0, 0)
BLUE = (26, 45, 196)

x_left, x_right, y_bottom, y_top = -1, 3, -1, 3  # Область определения f

N, M = -1, -1


def f(x, y):
    return math.cos(x * y) / 2 + 5


def coord_x(x, y, z):
    return (y - x) * math.sqrt(3) / 2


def coord_y(x, y, z):
    return (x + y) / 2 - z


def main():
    img = Image.new('RGB', (mx, my), color=WHITE)
    pixels = img.load()

    # draw(get_2d_coords_x, pixels)
    draw(pixels, need_y=True)

    img.show()


def draw(pixels, need_x=True, need_y=True):
    global N, M
    if need_x:
        N, M = 50, my * 2
        # == fix x ==
        minx = 10_000
        maxx = -minx
        miny, maxy = minx, maxx

        # global top_bound, bottom_bound
        top_bound, bottom_bound = [my] * mx, [0] * mx

        for x_2d, y_2d in get_2d_coords_fix_x():
            maxx, minx = max(x_2d, maxx), min(x_2d, minx)
            maxy, miny = max(y_2d, maxy), min(y_2d, miny)

        for x_2d, y_2d in get_2d_coords_fix_x():
            xx = int((x_2d - minx) / (maxx - minx) * (mx - 1))
            yy = int((y_2d - miny) / (maxy - miny) * (my - 1))

            if yy > bottom_bound[xx]:
                pixels[xx, yy] = RED
                bottom_bound[xx] = yy

            if yy < top_bound[xx]:
                pixels[xx, yy] = BLUE
                top_bound[xx] = yy

    if need_y:
        # == fix y ==
        N, M = 50, mx * 2

        minx = 1_000_000
        maxx = -minx
        miny, maxy = minx, maxx
        top_bound, bottom_bound = [my] * mx, [0] * mx

        for x_2d, y_2d in get_2d_coords_fix_y():
            maxx, minx = max(x_2d, maxx), min(x_2d, minx)
            maxy, miny = max(y_2d, maxy), min(y_2d, miny)

        for x_2d, y_2d in get_2d_coords_fix_y():
            xx = int((x_2d - minx) / (maxx - minx) * (mx - 1))
            yy = int((y_2d - miny) / (maxy - miny) * (my - 1))

            if yy > bottom_bound[xx]:
                pixels[xx, yy] = RED
                bottom_bound[xx] = yy

            if yy < top_bound[xx]:
                pixels[xx, yy] = BLUE
                top_bound[xx] = yy


def get_2d_coords_fix_x():
    print(N, M)
    for i in range(N + 1):
        x = x_right + i * (x_left - x_right) / N
        for j in range(M + 1):
            y = y_top + j * (y_bottom - y_top) / M
            z = f(x, y)

            x_2d = coord_x(x, y, z)
            y_2d = coord_y(x, y, z)

            yield x_2d, y_2d


def get_2d_coords_fix_y():
    print(N, M)
    for j in range(N + 1):
        y = y_top + j * (y_bottom - y_top) / N

        for i in range(M + 1):
            x = x_right + i * (x_left - x_right) / M
            z = f(x, y)

            x_2d = coord_x(x, y, z)
            y_2d = coord_y(x, y, z)

            yield x_2d, y_2d


if __name__ == '__main__':
    x_left, x_right, y_bottom, y_top = -2, 4, -3, 2
    main()
