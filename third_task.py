from typing import List

from util import GUI, Polygon, HTML_COLORS
from stubs import ThirdTaskStubs


def make_triangulation(polygon: Polygon) -> List[Polygon]:
    # 1. Проверка многоугольника на выпуклость
    # 1.1. Проверка принадлежности отрезка многоугольнику +
    # 1.1.1. Пересекаются ли два отрезка +
    # 1.1.2. Пересекаются ли стороны с отрезком (k - 1, k + 1) +
    # 1.1.3. Принадлежит ли середина многоугольнику +
    # 1.2. Разбиение выпуклого многоугольника диагоналями +
    # 2. Невыпуклый многоугольник
    # 2.1. В невыпуклой вершине найти выпуклый отрезок и
    #      по нему разбить исходный многоугольник разбить на два других.
    # 2.1.1 Как определять направленность отрезка
    # 2.2. Повторять 2.1. рекурсивно, пока не получили треугольник.
    non_convex_vertex = polygon.get_non_convex_vertex()
    if not non_convex_vertex:
        return polygon.split_with_diagonals()

    convex_segment = polygon.get_convex_segment(non_convex_vertex)
    polygon1, polygon2 = polygon.split_by_segment(convex_segment)

    triangles = []
    triangles.extend(make_triangulation(polygon1))
    triangles.extend(make_triangulation(polygon2))

    return triangles


def main():
    gui = GUI()
    canvas = gui.get_canvas()

    triangles = make_triangulation(ThirdTaskStubs.NON_CONVEX_POLYGON_4)
    if triangles is None:
        print(None)
        return

    for triangle in triangles:
        triangle.draw(canvas)
    # v = ThirdTaskStubs.NON_CONVEX_POLYGON_2.get_non_convex_vertex()
    # print(v)
    #
    # ThirdTaskStubs.NON_CONVEX_POLYGON_1.draw(canvas)

    gui.run()


if __name__ == '__main__':
    main()
