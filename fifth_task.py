"""
Вершины (неважно для чего) должны перечисляться последовательно,
чтобы не возникло самопересечений.

В ходе выполнения построения первая и последняя, а также все соседние, вершины
будут образовывать ребро, поэтому нужно следить, чтобы они не пересекали друг друга.
"""
from collections import Counter, defaultdict
from tkinter import BOTH
from typing import List, Tuple

from util import GUI, Polygon, Polyhedron, Plane, Segment3d, Segment2d, Point2d
from stubs import FifthTaskStubs


WIDTH = 700
HEIGHT = 700


def get_z_buffer(polyhedron: Polyhedron) -> List[List[List[int]]]:
    # 1. Пересечение плоскости и грани
    # 2. Нахождение точек пересечения отрезков
    # 3. Выбор масштаба (либо рисовать весь многогранник во весь экран +,
    #                    либо рисовать многогранник относительно экрана - возможно вылезание за экран)
    #          Видимо, выберу первый вариант, но нужно составить архитектуру так,
    #                  чтобы при желании было легко поменять "парадигму"
    z_buffer = [[[-1, -1] for x in range(WIDTH)] for y in range(HEIGHT)]  # Чем меньше значение, тем ближе

    # Если будет нужно учитывать реальный масштаб, то нужно будет поменять только эту строчку
    polyhedron.scale(WIDTH, HEIGHT)
    lengths = Counter()
    all_segs = defaultdict(list)
    for y in range(HEIGHT):
        intersect_2d_segments: List[Segment2d] = []
        for surface in polyhedron.surfaces:
            # Пересечение плоскости XOZ и грани
            xoz_plane = Plane([(0, y, 0), (1, y, 0), (0, y, 1)])  # Плоскость XOZ
            segments3d = xoz_plane.get_segments_on_plane(surface)
            all_segs[len(segments3d)].append(y)

            # Преобразовываем получившийся отрезок в двумерный на плоскости XOZ
            for segment3d in segments3d:
                segment3d.order_by_x()

                start_point = segment3d.start_point[0], segment3d.start_point[2]
                end_point = segment3d.end_point[0], segment3d.end_point[2]

                seg = Segment2d(start_point, end_point)
                seg.num = surface.num
                intersect_2d_segments.append(seg)

        # Отсортировать линейно по x начальной точки
        intersect_2d_segments.sort(key=lambda seg: seg.start_point[0])

        # Для каждого отрезка находим расстояния в z_buffer
        invert_y = HEIGHT - 1 - y
        for segment2d in intersect_2d_segments:
            start_x, end_x = int(segment2d.start_point[0]), int(segment2d.end_point[0])
            lengths.update([end_x - start_x])

            for x in range(start_x, end_x):
                dist = int(segment2d.get_dist(x))
                z_value = z_buffer[invert_y][x][0]
                if z_value == -1 or (dist != -1 and dist < z_value):
                    z_buffer[invert_y][x][0] = dist
                    z_buffer[invert_y][x][1] = segment2d.num

    return z_buffer


def main():
    polyhedron = Polyhedron(FifthTaskStubs.CONVEX_POLYHEDRON_3)
    z_buffer = get_z_buffer(polyhedron)

    gui = GUI(WIDTH, HEIGHT)
    gui.map_z_buffer(z_buffer)
    gui.run()


if __name__ == '__main__':
    main()
