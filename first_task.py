import pygame
import math


COLORS = {
    'black': (0, 0, 0),
    'red': (255, 0, 0),
    'green': (0, 255, 0),
    'blue': (0, 0, 255),
    'white': (255, 255, 255)
}


class Drawer:
    def __init__(self, width, height, a=-1, b=1, func=None):
        self.src_func = func

        self._width = width
        self._height = height

        self._left_border = a if a is not None else 0
        self._right_border = b if b is not None else 0

        self._screen = pygame.display.set_mode((width, height))
        self._clock = pygame.time.Clock()

        self._is_running = True

        pygame.init()

    def start(self):
        while self._is_running:
            self._handle_events()
            self._screen.fill(COLORS['white'])  # bg color

            self._draw_func()

            pygame.display.update()
            self._clock.tick(1000)

    def _draw_func(self):
        # Подготовка
        func = self.src_func

        a, b = self._left_border, self._right_border
        maxx, maxy = self._width, self._height
        ymin = ymax = func(a)

        # Находим границы по высоте
        for xx in range(maxx - 1):
            x = a + xx * (b - a) / maxx
            y = func(x)

            ymin = y if y < ymin else ymin
            ymax = y if y > ymax else ymax

        # Рисуем оси, если нужно
        # По y
        if a * b <= 0:
            self._draw_y_axis(self._transform(0, a, b, 0, maxx))

        # По y
        if ymin * ymax <= 0:
            self._draw_x_axis(maxy - self._transform(0, ymin, ymax, 0, maxy))

        # Рисуем саму функцию
        yy_last = self._calc_yy(func(a), ymin, ymax, maxy)
        for xx in range(1, maxx - 1):
            x = a + xx * (b - a) / maxx
            yy = self._calc_yy(func(x), ymin, ymax, maxy)

            self._draw_line(xx - 1, yy_last, xx, yy, COLORS['black'])

            yy_last = yy

    def _transform(self, old_x, old_min, old_max, new_min, new_max):
        return (old_x - old_min) * (new_max - new_min) / (old_max - old_min) + new_min

    def _calc_xx(self, x, a, b, maxx):
        return int((x - a) / (b - a) * maxx)

    def _calc_yy(self, y, ymin, ymax, maxy):
        return int((y - ymax) * maxy / (ymin - ymax))

    def _draw_point(self, x, y, color=COLORS['black']):
        x += self._x_center
        y = self._y_center - y

        self._draw_raw_point(x, y, color)

    def _draw_x_axis(self, y_mid):
        self._draw_line(0, y_mid, self._width, y_mid)

    def _draw_y_axis(self, x_mid):
        self._draw_line(x_mid, 0, x_mid, self._height)

    def _draw_line(self, x1, y1, x2, y2, color=COLORS['red']):
        pygame.draw.line(self._screen, color, (x1, y1), (x2, y2))

    def _draw_raw_point(self, x, y, color=COLORS['red']):
        self._screen.set_at((x, y), color)

    def _handle_events(self):
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                self._is_running = False


if __name__ == '__main__':
    funcs = {
        'xsin': lambda x: x * math.sin(x * x),
        'sqx': lambda x: x * x - 2,


        'tmp': lambda x: x ** 3 + 3 * x * x + 3 * x + 1
    }

    drawer = Drawer(800, 600, a=-2, b=5, func=funcs['xsin'])
    drawer.start()
