from collections import Counter
from tkinter import Tk, Canvas, BOTH, PhotoImage, Label
from typing import Tuple, List, Set
import numpy as np

RGD_COLORS = {
    'black': (0, 0, 0),
    'red': (255, 0, 0),
    'green': (0, 255, 0),
    'blue': (0, 0, 255),
    'white': (255, 255, 255)
}

HTML_COLORS = {
    'black': '#000',
    'red': '#f00',
    'green': '#0f0',
    'blue': '#00f',
    'white': '#fff',
    'light_purple': '',
    'dark_purple': '',
}

SIDE_COLORS = ['#E9967A', '#32CD32', '#00FA9A', '#808000', '#008B8B', '#FFFF00', '#8A2BE2', '#D2691E', '#696969', '#FFDEAD']

Point2d = Tuple[int, int]
Point3d = Tuple[int, int, int]


def orient_area(point_1: Point2d, point_2: Point2d, point_3: Point2d):
    return ((point_2[0] - point_1[0]) * (point_3[1] - point_1[1]) -
            (point_2[1] - point_1[1]) * (point_3[0] - point_1[0]))


class Segment2dBank:
    def __init__(self, segments: List['Segment2d']):
        self.segments: List['Segment2d'] = segments

    def get_segment(self, start_x: int) -> Tuple[int, 'Segment2d']:
        for i, segment in enumerate(self.segments):
            if segment.start_point[0] == start_x:
                return i, segment


class Segment2d:
    def __init__(self, start_point: Point2d, end_point: Point2d):
        self.start_point: Point2d = start_point
        self.end_point: Point2d = end_point
        self.num = None

    def get_center(self) -> Point2d:
        return ((self.start_point[0] + self.end_point[0]) // 2,
                (self.start_point[1] + self.end_point[1]) // 2)

    def get_point_set(self) -> Set[Point2d]:
        return {self.start_point, self.end_point}

    def draw(self, canvas: Canvas, fill_color=HTML_COLORS['red']):
        canvas.create_line(
            self.start_point, self.end_point, fill=fill_color, width=2)

        canvas.pack(fill=BOTH, expand=1)

    def get_dist(self, x: int) -> float:
        start_x, end_x = self.start_point[0], self.end_point[0]
        start_y, end_y = self.start_point[1], self.end_point[1]
        if start_x > end_x:
            start_x, end_x = self.end_point[0], self.start_point[0]
            start_y, end_y = self.end_point[1], self.start_point[1]

        if not (start_x <= x <= end_x):
            return -1

        try:
            _x, _y = (end_x - start_x), (end_y - start_y)
            return ((x - start_x) * _y + _x * start_y) / _x
            # coef = x / (end_x - start_x)
            # return coef * (end_y - start_y)
        except:
            print(x, self.start_point, self.end_point)
            raise

    def is_intersecting(self, other_segment: 'Segment2d') -> bool:
        area_1 = (orient_area(self.start_point, self.end_point, other_segment.start_point) *
                  orient_area(self.start_point, self.end_point, other_segment.end_point))
        area_2 = (orient_area(other_segment.start_point, other_segment.end_point, self.start_point) *
                  orient_area(other_segment.start_point, other_segment.end_point, self.end_point))
        return (self._check_need_intersect_condition(other_segment, 0) and
                self._check_need_intersect_condition(other_segment, 1) and
                (area_1 <= 0) and (area_2 <= 0))

    def _check_need_intersect_condition(self, other_segment: 'Segment2d', i: int) -> bool:
        min1x, max1x = (min(self.start_point[i], self.end_point[i]),
                        max(self.start_point[i], self.end_point[i]))
        min2x, max2x = (min(other_segment.start_point[i], other_segment.end_point[i]),
                        max(other_segment.start_point[i], other_segment.end_point[i]))
        return max(min1x, min2x) <= min(max1x, max2x)


class Segment3d:
    def __init__(self, start_point: Point3d, end_point: Point3d):
        self.start_point: Point3d = start_point
        self.end_point: Point3d = end_point

    def order_by_x(self):
        if self.start_point[0] > self.end_point[0]:
            self.start_point, self.end_point = self.end_point, self.start_point

    def get_intersect_points(self, other: 'Segment3d') -> List[Point3d]:
        pass


class Polygon:
    def __init__(self, vertexes):
        self.vertex_count: int = len(vertexes)
        self.vertexes: List[Tuple[int, int]] = vertexes
        vertex_count = self.vertex_count

        self.segments = []
        for i in range(vertex_count):
            self.segments.append(
                Segment2d(vertexes[i], vertexes[(i + 1) % vertex_count]))

        self.hoards: List[List[Segment2d]] = [
            [Segment2d(vertexes[i], vertexes[j]) for j in range(vertex_count)]
            for i in range(vertex_count)]

    def draw(self, canvas: Canvas, outline_color=HTML_COLORS['black'], bg_color=HTML_COLORS['red']):
        canvas.create_polygon(
            self.vertexes, fill=bg_color, outline=outline_color, width=2)

        canvas.pack(fill=BOTH, expand=1)

    def is_triangle(self) -> bool:
        return len(self.vertexes) == 3

    def get_non_convex_vertex(self):
        if self.vertex_count <= 3:
            return

        for k in range(self.vertex_count):
            if not self.is_convex_vertex(k):
                return k

    def is_convex_vertex(self, k):
        hoard = self.hoards[(k - 1) % self.vertex_count][(k + 1) % self.vertex_count]
        if self.is_intersecting(hoard):
            return False
        return self.is_inside(hoard.get_center())

    def is_intersecting(self, out_segment: Segment2d):
        for segment in self.segments:
            if out_segment.is_intersecting(segment):
                if not out_segment.get_point_set().intersection(segment.get_point_set()):
                    return True
        return False

    def is_inside(self, point: Tuple[int, int]):
        c = 0
        xp = [x for (x, y) in self.vertexes]
        yp = [y for (x, y) in self.vertexes]
        x, y = point
        for i in range(len(xp)):
            if (((yp[i] <= y < yp[i - 1]) or (yp[i - 1] <= y < yp[i])) and
                    (x > (xp[i - 1] - xp[i]) * (y - yp[i]) / (yp[i - 1] - yp[i]) + xp[i])):
                c = 1 - c
        return c

    def split_with_diagonals(self) -> List['Polygon']:
        triangles = []
        for i in range(1, self.vertex_count - 1):
            vertexes = [self.vertexes[0], self.vertexes[i], self.vertexes[i + 1]]
            triangles.append(Polygon(vertexes))

        return triangles

    def get_convex_segment(self, non_convex_vertex: int) -> Segment2d:
        exclude_vertexes = {non_convex_vertex,
                            (non_convex_vertex - 1) % self.vertex_count,
                            (non_convex_vertex + 1) % self.vertex_count}

        for v in range(self.vertex_count):
            if v in exclude_vertexes:
                continue

            segment = Segment2d(self.vertexes[v], self.vertexes[non_convex_vertex])
            if self.is_intersecting(segment):
                continue
            if self.is_inside(segment.get_center()):
                return segment

    def split_by_segment(self, segment: Segment2d) -> Tuple['Polygon', 'Polygon']:
        if segment.start_point not in self.vertexes or segment.end_point not in self.vertexes:
            raise Exception('Начальная точка должна быть вершиной')

        start_index = self.vertexes.index(segment.start_point)
        end_index = self.vertexes.index(segment.end_point)
        start_index, end_index = min(start_index, end_index), max(start_index, end_index)

        vertexes_1 = self.vertexes[start_index:end_index + 1]
        vertexes_2 = self.vertexes[0:start_index + 1] + self.vertexes[end_index:]
        return Polygon(vertexes_1), Polygon(vertexes_2)


class Surface:
    def __init__(self, surface_vertexes: List[Point3d], num):
        self.vertexes: List[Point3d] = surface_vertexes
        self.polygon: Polygon = Polygon(surface_vertexes)
        self.num = num
        self.__maxes_are_actual = False
        self.__maxes: Tuple[int, int, int] = (None, None, None)
        self.__mines_are_actual = False
        self.__mines: Tuple[int, int, int] = (None, None, None)

    def get_maxes(self) -> Tuple[int, int, int]:
        if not self.__maxes_are_actual:
            self.find_out_maxes()
        return self.__maxes

    def find_out_maxes(self):
        maxes = [0, 0, 0]
        for point3d in self.vertexes:
            for i in range(3):
                if maxes[i] < point3d[i]:
                    maxes[i] = point3d[i]

        self.__maxes_are_actual = True
        self.__maxes = tuple(maxes)

    def get_mines(self) -> Tuple[int, int, int]:
        if not self.__mines_are_actual:
            self.find_out_mines()
        return self.__mines

    def find_out_mines(self):
        mines = [float('inf'), float('inf'), float('inf')]
        for point3d in self.vertexes:
            for i in range(3):
                if mines[i] > point3d[i]:
                    mines[i] = point3d[i]

        self.__mines_are_actual = True
        self.__mines = tuple(mines)


class Plane:
    def __init__(self, plane_vertexes: List[Tuple[int, int, int]]):
        self.vertexes: List[Tuple[int, int, int]] = plane_vertexes

    def plane_equation(self, X):
        A, B, C = self._get_plane_points()

        x, y, z = X[0], X[1], X[2]
        x0, y0, z0 = A[0], A[1], A[2]
        x1, y1, z1 = B[0], B[1], B[2]
        x2, y2, z2 = C[0], C[1], C[2]

        matrix = np.array([[x - x0,  y - y0,  z - z0],
                           [x1 - x0, y1 - y0, z1 - z0],
                           [x2 - x0, y2 - y0, z2 - z0]])
        return np.linalg.det(matrix)

    def _get_plane_points(self) -> Tuple[np.array, np.array, np.array]:
        A = np.array(self.vertexes[0])
        B = np.array(self.vertexes[1])
        C = np.array(self.vertexes[2])
        return A, B, C

    def get_segments_on_plane(self, surface: Surface) -> List[Segment3d]:
        A, B, C = self._get_plane_points()

        N = np.cross(B - A, C - A)
        N = np.array([int(x) for x in (N / np.linalg.norm(N))])

        surface_len = len(surface.vertexes)
        points = [(np.array(surface.vertexes[i]), np.array(surface.vertexes[(i + 1) % surface_len]))
                  for i in range(surface_len)]

        prepoint: Point3d = None
        result_segments: List[Segment3d] = []
        for X, Y in points:
            X_num, Y_num = self.plane_equation(X), self.plane_equation(Y)

            # Находятся на плоскости
            if X_num == 0 and Y_num == 0:
                result_segments.append(Segment3d(tuple(X), tuple(Y)))
                continue

            if X_num == 0 or Y_num == 0:
                continue

            # Находятся с одной стороны
            if int(X_num / abs(X_num)) == int(Y_num / abs(Y_num)):
                continue

            V = A - X
            d = N.dot(V)
            W = Y - X
            e = N.dot(W)

            if e != 0:
                if prepoint:
                    result_segments.append(Segment3d(prepoint, tuple(X + W * d / e)))
                    prepoint = None
                else:
                    prepoint = tuple(X + W * d / e)
        return result_segments


class Polyhedron:
    def __init__(self, surfaces_vertexes: List[List[Point3d]]):
        self.surface_count: int = len(surfaces_vertexes)

        self.__maxes_are_actual = False
        self.__maxes: Tuple[int, int, int] = (None, None, None)

        self.surfaces = []
        for i, surface_vertexes in enumerate(surfaces_vertexes):
            self.surfaces.append(Surface(surface_vertexes, i))

    def get_maxes(self) -> Tuple[int, int, int]:
        if not self.__maxes_are_actual:
            self.find_out_maxes()
        return self.__maxes

    def find_out_maxes(self):
        maxes = [0, 0, 0]
        for surface in self.surfaces:
            surface_maxes = surface.get_maxes()
            for i in range(3):
                if maxes[i] < surface_maxes[i]:
                    maxes[i] = surface_maxes[i]

        self.__maxes_are_actual = True
        self.__maxes = tuple(maxes)

    def scale(self, width, height):
        new_min, coef = float('inf'), float('inf')
        for old, new in zip(self.get_maxes()[:2], [width, height]):
            if new_min > new:
                new_min, coef = new, new / old

        new_surfaces = []
        for surface in self.surfaces:
            new_vertexes = []
            for point3d in surface.vertexes:
                new_vertexes.append(tuple(map(lambda x: int(x * coef), point3d)))
            new_surfaces.append(Surface(new_vertexes, surface.num))

        self.surfaces = new_surfaces
        self.__maxes = tuple(map(lambda x: int(x * coef), self.get_maxes()))


class GUI:
    def __init__(self, width, height):
        self.root = Tk()
        self.root.geometry("{}x{}+600+325".format(height, width))
        self.width = width
        self.height = height
        self.image = PhotoImage(width=width, height=height)
        label = Label(self.root, image=self.image, borderwidth=2, relief="raised")
        label.pack(fill="both", expand=True)

    def get_canvas(self):
        return Canvas(self.root)

    def map_z_buffer(self, z_buffer: List[List[List[int]]]):
        # Находим минимум и максимум
        m = float('+inf')
        M = float('-inf')
        for l in z_buffer:
            for dist, num in l:
                if dist != -1 and m > dist:
                    m = dist
                if dist != -1 and M < dist:
                    M = dist
        M = M if M else 1

        # Вычитаем минимум
        for y in range(len(z_buffer)):
            for x in range(len(z_buffer[y])):
                if z_buffer[y][x][0] != -1:
                    z_buffer[y][x][0] -= m

        a = (255 - 60) / (M - m)
        b = 60

        print('a ', a)
        print('b ', b)
        print('M ', M)
        colors = []
        for l in z_buffer:
            colors.append([])
            for dist, num in l:

                # if dist == -1:
                #     colors[-1].append('#fff')
                # else:
                #     color = hex(int(a * dist + b)).split('x')[-1]
                #     color = (color + color) if len(color) == 1 else color
                #     colors[-1].append('#{0}00{0}'.format(color))
                if num == -1:
                    colors[-1].append('#fff')
                else:
                    colors[-1].append(SIDE_COLORS[num])

        for y, l in enumerate(colors):
            # if i >= 50:
            #     continue
            for x, color in enumerate(l):
                # if j >= 50:
                #     continue
                try:
                    self.image.put((color,), to=(x, y))
                except:
                    print(color)

    def run(self):
        self.root.mainloop()
