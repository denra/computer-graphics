from enum import Enum
from math import *
from tkinter import Tk, Canvas, LAST


class Directions(Enum):
    vertical = 1
    horizontal = 2
    diagonal = 3


class Point:
    def __init__(self, x, y):
        self.x = x
        self.y = y

    def first_var(self, invert):
        if half_p > 0:
            return Point(self.x - 1, self.y - 1)
        else:
            return Point(self.x + 1, self.y + 1)

    def second_var(self, invert):
        if half_p > 0:
            if invert:
                return Point(self.x, self.y - 1)
            else:
                return Point(self.x, self.y + 1)
        else:
            if invert:
                return Point(self.x - 1, self.y + 1)
            else:
                return Point(self.x + 1, self.y - 1)

    def third_var(self, invert):
        if half_p > 0:
            if invert:
                return Point(self.x + 1, self.y - 1)
            else:
                return Point(self.x - 1, self.y + 1)
        else:
            if invert:
                return Point(self.x, self.y + 1)
            else:
                return Point(self.x + 1, self.y)

    def __round__(self):
        self.x = round(self.x)
        self.y = round(self.y)

    def __str__(self):
        return "X: %s Y: %s" % (self.x, self.y)


param_a = 1
param_b = 2
param_c = 0.01

root = Tk()
width = 1300
height = 800
canvas = Canvas(root, width=width, height=height, bg="white")


def create_axis():
    canvas.create_line(0, center_y, width, center_y, width=2, arrow=LAST)
    canvas.create_line(center_x, height, center_x, 0, width=2, arrow=LAST)


def get_distance_to_focus(point):
    return sqrt(pow(point.x - focus.x, 2) + pow(point.y - focus.y, 2))


def get_foc_dir_delta(point):
    focus_dist = get_distance_to_focus(point)
    dir_dist = directris_distance(point)
    return abs(focus_dist - dir_dist)


def get_dir():
    return 1 if half_p > 0 else -1


def bresenh(current_point, color, invert):
    diag = get_foc_dir_delta(current_point.third_var(invert))
    vert = get_foc_dir_delta(current_point.second_var(invert))
    hor = get_foc_dir_delta(current_point.first_var(invert))

    actions = {diag: Directions.diagonal,
               vert: Directions.vertical,
               hor: Directions.horizontal}

    res = actions[min(actions.keys())]

    if res == Directions.diagonal:
        current_point.x = current_point.third_var(invert).x
        current_point.y = current_point.third_var(invert).y
    elif res == Directions.vertical:
        current_point.x = current_point.second_var(invert).x
        current_point.y = current_point.second_var(invert).y
    else:
        current_point.x = current_point.first_var(invert).x
        current_point.y = current_point.first_var(invert).y

    canvas.create_rectangle(current_point.x, current_point.y,
                            current_point.x + 1, current_point.y + 1,
                            outline=color, width=1)


center_x = width / 2
center_y = height / 2


angle = 5 * pi / 4


def transform_point(p):
    p.x -= delta_x
    p.y += delta_y
    return Point(p.x * cos(angle) - p.y * sin(angle) + center_x,
                 p.x * sin(angle) + p.y * cos(angle) + center_y)


if param_c != 0 and param_b != param_a and param_b != -param_a and param_a != 0 and param_b != 0:
    delta_y = eval("-sqrt(2) * (({a} + {b}) ** 2) / (16 * {c} * ({b} - {a}))".format(a=param_a, b=param_b, c=param_c))
    delta_x = eval("sqrt(2) * ({a} + {b}) / (8 * {c})".format(a=param_a, b=param_b, c=param_c))
    half_p = eval("(sqrt(2) * ({a} - {b})) / (16 * {c})".format(a=param_a, b=param_b, c=param_c))
    focus = transform_point(Point(half_p, 0))
    minus_focus = transform_point(Point(-half_p, 0))

    canvas.create_line(focus.x, focus.y, minus_focus.x, minus_focus.y)

    directris_first = transform_point(Point(-half_p, 0))
    directris_second = transform_point(Point(-half_p, height))

    canvas.create_line(directris_first.x, directris_first.y, directris_second.x,
                       directris_second.y)

    sign = lambda x: 1 if x >= 0 else -1


def get_coeffs(first_point, second_point):
    a_coeff = first_point.y - second_point.y
    b_coeff = second_point.x - first_point.x
    c_coeff = first_point.x * second_point.y - first_point.y * second_point.x
    norm_mult = sign(c_coeff) * 1 / sqrt(pow(a_coeff, 2) + pow(b_coeff, 2))
    return a_coeff, b_coeff, c_coeff, norm_mult


def directris_distance(point: Point):
    a, b, c, norm_mult = get_coeffs(directris_first, directris_second)
    return abs((a * point.x + b * point.y + c) * norm_mult)


def should_go(y):
    if half_p > 0:
        return y > -height
    else:
        return y < height * 2


def process(color, invert):
    point = transform_point(Point(0, 0))
    canvas.create_rectangle(point.x, point.y, point.x + 1, point.y + 1,
                            outline=color)
    while should_go(point.y):
        print(point)
        bresenh(point, color, invert)


if __name__ == '__main__':
    create_axis()
    if param_c == 0:
        if param_a == 0 and param_b == 0:
            canvas.create_line(center_x, center_y, center_x, center_y)
        elif param_a == 0:
            canvas.create_line(0, center_y, width, center_y, fill='red')
        elif param_b == 0:
            canvas.create_line(center_x, 0, center_x, height, fill='red')
    else:
        process('red', True)
        process('red', False)


    canvas.pack()
    root.mainloop()
