from PIL import Image


A = -3
B = -91
C = -0.01

HEIGHT = 480
WIDTH = 640

WHITE = (255, 255, 255)
BLACK = (0, 0, 0)
RED = (255, 0, 0)


def main():
    img = Image.new('RGB', (WIDTH, HEIGHT), color=WHITE)
    pixels = img.load()
    draw_axis(pixels)
    
    current = Point(-B, C)
        
    if A == 0:
        draw_line(current, pixels)
        img.show()
        return
        
    delta = 0

    if A < 0:
        dx = Point(-1, 0)
        dy = Point(0, -1)
        dy_del = lambda d, p: d - 1
        dx_del = lambda d, p: d + 2 * A * (p.x + B) - A
        test = lambda p: (p.y + HEIGHT // 2) > 0
    else:
        dx = Point(1, 0)
        dy = Point(0, 1)
        dy_del = lambda d, p: d + 1
        dx_del = lambda d, p: d - 2 * A * (p.x + B) - A
        test = lambda p: (p.y - HEIGHT // 2) < 0
    dxdy = dx + dy
    dxdy_del = lambda d, p: dy_del(dx_del(d, p), p)
        
    while test(current):
        draw_pixel(current, pixels)
        
        curr_dx = abs(dx_del(delta, current))
        curr_dy = abs(dy_del(delta, current))
        curr_dxdy = abs(dxdy_del(delta, current))
        
        curr_min = min(min(curr_dx, curr_dy), curr_dxdy)
        if curr_min == curr_dx:
            current = current + dx
            delta = dx_del(delta, current)
        elif curr_min == curr_dy:
            current = current + dy
            delta = dy_del(delta, current)
        else:
            current = current + dxdy
            delta = dxdy_del(delta, current)
    
    img.show()


def draw_pixel(current, pixels):
    try:
        loc_current = Point(current.x + WIDTH // 2, HEIGHT // 2 - current.y)
        pixels[loc_current.x, loc_current.y] = BLACK
    except:
        print(current)
        
    try:
        loc_current = Point((- (current.x + B)) - B + WIDTH // 2, HEIGHT // 2 - current.y)
        pixels[loc_current.x, loc_current.y] = BLACK
    except:
        print(current)
        
        
def draw_line(current, pixels):
    for xx in range(WIDTH):
        pixels[xx, HEIGHT // 2 - current.y] = BLACK


def draw_axis(pixels):
    for xx in range(WIDTH):
        pixels[xx, HEIGHT // 2] = RED
    for yy in range(HEIGHT):
        pixels[WIDTH // 2, yy] = RED


class Point:

    def __init__(self, x, y):
        self.x = x
        self.y = y
        
    def __add__(self, other):
        return Point(self.x + other.x, self.y + other.y)
        
    def __str__(self):
        return 'x={0},y={1}'.format(self.x, self.y)


if __name__ == '__main__':
    main()
